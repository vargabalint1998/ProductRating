package cyzz.springboot.studies.productRating;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

import com.vaadin.spring.annotation.EnableVaadin;

@EnableAspectJAutoProxy
@ComponentScan("cyzz.springboot.studies.productRating")
@SpringBootApplication
@EnableVaadin
public class ProductRatingApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProductRatingApplication.class, args);
	}
}
