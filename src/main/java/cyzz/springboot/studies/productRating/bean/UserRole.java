package cyzz.springboot.studies.productRating.bean;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;

import org.springframework.security.core.GrantedAuthority;

@Entity
public class UserRole implements GrantedAuthority {

	/**
	 *
	 */
	private static final long serialVersionUID = 5056245919399831812L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_GEN_R")
	@SequenceGenerator(name = "SEQ_GEN_R", sequenceName = "s_roles", allocationSize = 1, initialValue = 1)
	private Long id;

	@Column(name = "authority", length = 100, nullable = false)
	private String authority;

	@ManyToMany(mappedBy = "roles")
	private Set<User> users = new HashSet<>();

	public UserRole(String role) {
		this.authority = role;
	}

	@Override
	public String getAuthority() {
		return this.authority;
	}

	public Set<User> getUsers() {
		return Collections.unmodifiableSet(this.users);
	}

	public UserRole() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "UserRole [authority=" + this.authority + "]";
	}

}
