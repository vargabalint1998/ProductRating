package cyzz.springboot.studies.productRating.vaadin.window;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.vaadin.dialogs.ConfirmDialog;
import org.vaadin.spring.security.shared.VaadinSharedSecurity;

import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.renderers.ButtonRenderer;

import cyzz.springboot.studies.productRating.bean.Product;
import cyzz.springboot.studies.productRating.bean.Review;
import cyzz.springboot.studies.productRating.bean.User;
import cyzz.springboot.studies.productRating.service.ReviewService;
import cyzz.springboot.studies.productRating.service.UserService;

@SpringComponent
@Scope("prototype")
public class ProductInspectWindow extends Window {
	private static final long serialVersionUID = -1161833431948058284L;

	@Autowired
	private VaadinSharedSecurity security;

	@Autowired
	private ApplicationContext appCtx;

	@Autowired
	UserService userService;

	@Autowired
	private ReviewService service;

	private Grid<Review> grid;

	public void init(Product product) {

		this.setHeight("70%");
		this.setWidth("70%");
		this.center();
		this.setModal(true);

		this.setContent(this.createContentLayout(product));

	}

	private Component createContentLayout(Product product) {
		VerticalLayout mainLayout = new VerticalLayout();
		HorizontalLayout productDetailsLayout = new HorizontalLayout();
		VerticalLayout productInfo = new VerticalLayout();
		Label lblName, lblPrice, lblBuyDate, lblBuyLocation;
		lblName = new Label("Termék neve: " + product.getName());

		lblPrice = new Label("Temék ára: " + product.getPrice().toString());

		lblBuyDate = new Label();
		if (product.getBuyDate() == null) {
			lblBuyDate.setValue("Termék vásárlásának időpontja: Nem ismert.");
		} else {
			lblBuyDate.setValue("Termék vásárlásának időpontja: " + product.getBuyDate().toString());
		}
		lblBuyLocation = new Label("Vásárlás helye: " + product.getBuyLocation());
		Button btnAdd = new Button("Új elem hozzáadása");

		btnAdd.setSizeUndefined();

		btnAdd.addClickListener(e -> {
			ReviewInspectWindow window = new ReviewInspectWindow();
			Review r = new Review();
			r.setProduct(product);
			User user = this.userService.getUser(this.security.getAuthentication().getName());
			r.setUser(user);
			window.init(r, review -> {
				this.service.updateReview(review);
				window.close();
				this.refreshGrid();
			});
			UI.getCurrent().addWindow(window);

		});
		productInfo.addComponents(lblName, lblPrice, lblBuyLocation, lblBuyDate, btnAdd);
		productDetailsLayout.addComponents(productInfo, btnAdd);

		HorizontalLayout productReviewsAndProductDescription = new HorizontalLayout();
		TextArea productDescription = new TextArea("Termék leírása");
		productDescription.setValue(product.getDescription());
		productDescription.setReadOnly(true);

		this.grid = this.loadGrid(product);

		this.grid.setSizeFull();
		productReviewsAndProductDescription.addComponents(productDescription, this.grid);

		productDetailsLayout.setSizeFull();
		productReviewsAndProductDescription.setSizeFull();
		mainLayout.addComponents(productDetailsLayout, productReviewsAndProductDescription);

		mainLayout.setSizeFull();
		return mainLayout;
	}

	private Grid<Review> loadGrid(Product product) {
		Grid<Review> grid = new Grid<>(Review.class);

		grid.setItems(product.getReviews());

		grid.addColumn(e -> "Szerkesztés", new ButtonRenderer<>(r -> {
			Review rev = r.getItem();
			if (rev.getUser().getUsername().equals(this.security.getAuthentication().getName())) {
				ReviewInspectWindow window = this.appCtx.getBean(ReviewInspectWindow.class);
				window.init(r.getItem(), review -> {
					this.service.updateReview(review);
					window.close();
					this.refreshGrid();
				});
				UI.getCurrent().addWindow(window);

			} else {
				Notification.show("Nincs jogosultsága más véleményének szerkesztésére!", Type.WARNING_MESSAGE);
			}
		}));

		grid.addColumn(e -> "Törlés", new ButtonRenderer<>(r -> {
			ConfirmDialog.show(UI.getCurrent(), "Törlés", "Biztos törölni szeretnéd?", "Igen", "Mégse", window -> {
				if (window.isConfirmed()) {
					Review review = r.getItem();
					this.service.deleteReview(review);
					this.refreshGrid();
				}
			});
		}));

		grid.setSizeFull();
		return grid;
	}

	private void refreshGrid() {

		this.grid.setItems(this.service.listReviews());

	}

}
