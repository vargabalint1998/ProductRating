package cyzz.springboot.studies.productRating.vaadin.window;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;

import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Component;
import com.vaadin.ui.ListSelect;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

import cyzz.springboot.studies.productRating.bean.User;
import cyzz.springboot.studies.productRating.bean.UserRole;
import cyzz.springboot.studies.productRating.service.RoleService;

@SpringComponent
@Scope("prototype")
public class UserEditWindow extends Window {

	@Autowired
	RoleService roleService;
	SaveHandler handler;

	public void init(User user, SaveHandler saveHandler) {
		this.setHeight("70%");
		this.setWidth("70%");
		this.center();
		this.setModal(true);
		this.handler = saveHandler;

		this.setContent(this.createContentLayout(user));
	}

	private Component createContentLayout(User user) {
		VerticalLayout layout = new VerticalLayout();

		TextField txtId = new TextField("Id");
		TextField txtName = new TextField("Username");
		TextField txtEmail = new TextField("Email");

		txtId.setValue(user.getId().toString());
		txtName.setReadOnly(true);
		txtName.setValue(user.getUsername());
		txtId.setReadOnly(true);
		txtEmail.setReadOnly(true);
		txtEmail.setValue(user.getEmail());

		ListSelect<UserRole> roles = new ListSelect<>("Roleok");
		roles.setItems(this.roleService.getRoles());

		user.addRole(roles.getSelectedItems());

		this.handler.onSave(user);
		layout.addComponents(txtId, txtName, txtEmail, roles);
		layout.setSizeFull();
		return layout;
	}

	@FunctionalInterface
	public interface SaveHandler {
		public void onSave(User user);
	}
}
