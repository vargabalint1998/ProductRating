package cyzz.springboot.studies.productRating.vaadin.ui;

import org.springframework.beans.factory.annotation.Autowired;
import org.vaadin.spring.security.VaadinSecurity;

import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.spring.navigator.SpringNavigator;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import cyzz.springboot.studies.productRating.vaadin.view.AddProductView;
import cyzz.springboot.studies.productRating.vaadin.view.ProductView;

@SpringUI(path = "/")
public class MainUI extends UI {

	/**
	 *
	 */
	private static final long serialVersionUID = 1967442202802325189L;

	@Autowired
	private SpringNavigator navigator;

	@Autowired
	private VaadinSecurity vaadinSecurity;

	@Override
	protected void init(VaadinRequest request) {
		VerticalLayout mainLayout = new VerticalLayout();
		VerticalLayout productLayout = this.createProductLayout();
		HorizontalLayout menu = this.createMenu();

		mainLayout.addComponents(menu, productLayout);
		this.setContent(mainLayout);

		this.navigator.init(this, productLayout);

		if (this.navigator.getState().isEmpty()) {
			this.navigator.navigateTo(ProductView.VIEW_NAME);
		}

		this.setNavigator(this.navigator);
	}

	private HorizontalLayout createMenu() {
		HorizontalLayout menu = new HorizontalLayout();
		menu.setSizeFull();

		Button btnProductList = new Button();
		btnProductList.setCaption("Termék lista");
		btnProductList.addClickListener(e -> this.navigator.navigateTo(ProductView.VIEW_NAME));

		Button btnAddProduct = new Button();
		btnAddProduct.setCaption("Termék hozzáadása");
		btnAddProduct.addClickListener(e -> this.navigator.navigateTo(AddProductView.VIEW_NAME));

		Button btnLogout = new Button("Kijelentkezés", e -> this.vaadinSecurity.logout());

		btnProductList.addStyleName(ValoTheme.BUTTON_LINK);
		btnAddProduct.addStyleName(ValoTheme.BUTTON_LINK);
		btnLogout.addStyleName(ValoTheme.BUTTON_LINK);

		menu.addComponents(btnProductList, btnAddProduct, btnLogout);
		menu.setComponentAlignment(btnLogout, Alignment.MIDDLE_RIGHT);
		menu.setExpandRatio(btnLogout, 1F);
		return menu;
	}

	private VerticalLayout createProductLayout() {
		VerticalLayout layout = new VerticalLayout();

		layout.setSpacing(true);
		layout.setMargin(true);

		return layout;
	}

}
