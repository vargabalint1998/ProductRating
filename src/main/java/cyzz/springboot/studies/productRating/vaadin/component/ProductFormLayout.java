package cyzz.springboot.studies.productRating.vaadin.component;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import com.vaadin.data.Binder;
import com.vaadin.ui.Button;
import com.vaadin.ui.DateField;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;

import cyzz.springboot.studies.productRating.bean.Product;

public class ProductFormLayout extends FormLayout {

	private Product product;
	private SaveHandler saveHandler;

	public ProductFormLayout(SaveHandler saveHandler) {
		this(new Product(), saveHandler);
	}

	public ProductFormLayout(Product product, SaveHandler saveHandler) {
		this.product = product;
		this.saveHandler = saveHandler;

		this.loadContent();
	}

	private void loadContent() {
		this.product.setEnabled(false);

		this.setMargin(true);
		this.setSpacing(true);

		TextField txtId = new TextField("Azonosító");
		TextField txtName = new TextField("Termék neve");
		TextField txtBuyLocation = new TextField("Termék vásárlási helye");
		DateField dtFldBuyDate = new DateField("Termék vásárlásának dátuma");
		TextArea txtDescription = new TextArea("Leírás");
		TextField txtPrice = new TextField("Termék ára");

		dtFldBuyDate.setValue(LocalDate.now());

		txtId.setReadOnly(true);

		Binder<Product> binder = new Binder<>(Product.class);

		binder.forField(txtId).withNullRepresentation("")
				.withConverter(this::convertStringToInteger, this::convertIntegerToString).bind("id");

		binder.forField(txtName).asRequired("Név megadása kötelező")
				.withValidator(str -> str.length() <= 256, "A mező hossza nem lehet több 256 karakternél").bind("name");

		binder.forField(txtBuyLocation).withNullRepresentation("")
				.withValidator(str -> str.length() <= 128, "A mező hossza nem lehet több 128 karakternél")
				.bind("buyLocation");

		binder.forField(dtFldBuyDate).withConverter(this::convertLocalDateToDate, this::convertDateToLocalDate)
				.bind("buyDate");

		binder.forField(txtDescription)
				.withValidator(str -> str.length() <= 256, "A mező hossza nem lehet több 256 karakternél.")
				.bind("description");

		binder.forField(txtPrice).asRequired("Ennek a mezőnek a megadása kötelező!").withNullRepresentation("")
				.withValidator(str -> str.length() >= 3, "A mező értékének 3 karakterből kell állnia")
				.withConverter(this::convertStringToInteger, this::convertIntegerToString,
						"Ebbe a mezőbe csak számokat adjon meg!")
				.bind("price");

		binder.readBean(this.product);

		Button btnSave = new Button("Mentés");
		btnSave.addClickListener(e -> {
			if (binder.writeBeanIfValid(this.product)) {
				this.saveHandler.onSave(this.product);
			}
		});

		this.addComponents(txtId, txtName, txtBuyLocation, dtFldBuyDate, txtDescription, txtPrice, btnSave);

	}

	private LocalDate convertDateToLocalDate(Date date) {
		if (date != null) {
			LocalDate local = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
			return local;
		}
		return null;
	}

	private Date convertLocalDateToDate(LocalDate local) {
		if (local != null) {
			Date date = Date.from(local.atStartOfDay(ZoneId.systemDefault()).toInstant());
			return date;
		}
		return null;
	}

	private String convertIntegerToString(Integer i) {
		if (i != null) {
			return i.toString();
		}
		return null;
	}

	private Integer convertStringToInteger(String s) {
		if ((s != null) && !s.isEmpty()) {
			return Integer.valueOf(s);
		}
		return null;
	}

	@FunctionalInterface
	public interface SaveHandler {
		public void onSave(Product product);
	}

}
