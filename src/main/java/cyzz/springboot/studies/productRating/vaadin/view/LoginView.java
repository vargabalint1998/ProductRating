package cyzz.springboot.studies.productRating.vaadin.view;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.AuthenticationException;
import org.vaadin.spring.security.shared.VaadinSharedSecurity;

import com.vaadin.event.ShortcutAction;
import com.vaadin.navigator.View;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.Panel;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

@SpringView(name = LoginView.VIEW_NAME)
public class LoginView extends Panel implements View {
	private static final long serialVersionUID = -1717338520050439742L;
	public static final String VIEW_NAME = "loginView";

	private VaadinSharedSecurity vaadinSharedSecurity;

	private Label logoutLabel;
	private Label loginFailedLabel;

	private TextField txtUsername;
	private TextField txtPassword;
	private Button btnLogin;

	@Autowired
	public LoginView(VaadinSharedSecurity vaadinSharedSecurity) {
		this.vaadinSharedSecurity = vaadinSharedSecurity;
	}

	@PostConstruct
	public void init() {
		FormLayout loginForm = this.createLoginForm();

		VerticalLayout loginLayout = this.createLoginLayout(loginForm);

		VerticalLayout rootLayout = new VerticalLayout(loginLayout);
		rootLayout.setSizeFull();
		rootLayout.setComponentAlignment(loginLayout, Alignment.MIDDLE_CENTER);
		this.setContent(rootLayout);
		this.setSizeFull();
	}

	private VerticalLayout createLoginLayout(FormLayout loginForm) {
		VerticalLayout loginLayout = new VerticalLayout();

		/*
		 * if (request.getParameter("logout") != null) { this.logoutLabel = new
		 * Label("Sikeresen kijelentkezett!");
		 * this.logoutLabel.addStyleName(ValoTheme.LABEL_SUCCESS);
		 * this.logoutLabel.setSizeUndefined();
		 * loginLayout.addComponent(this.logoutLabel);
		 * loginLayout.setComponentAlignment(this.logoutLabel, Alignment.BOTTOM_CENTER);
		 * }
		 */
		loginLayout.addComponent(this.loginFailedLabel = new Label());
		loginLayout.setComponentAlignment(this.loginFailedLabel, Alignment.TOP_CENTER);
		this.loginFailedLabel.setSizeUndefined();
		this.loginFailedLabel.addStyleName(ValoTheme.LABEL_FAILURE);
		this.loginFailedLabel.setVisible(false);

		loginLayout.addComponent(loginForm);
		loginLayout.setComponentAlignment(loginForm, Alignment.TOP_CENTER);

		return loginLayout;
	}

	private FormLayout createLoginForm() {
		FormLayout loginForm = new FormLayout();
		loginForm.setSizeUndefined();
		loginForm.setSpacing(true);

		this.txtUsername = new TextField("Felhasználónév");
		this.txtPassword = new PasswordField("Jelszó");
		this.btnLogin = new Button("Belépés");
		loginForm.addComponents(this.txtUsername, this.txtPassword, this.btnLogin);

		this.btnLogin.addStyleName(ValoTheme.BUTTON_PRIMARY);
		this.btnLogin.setDisableOnClick(true);
		this.btnLogin.setClickShortcut(ShortcutAction.KeyCode.ENTER);
		this.btnLogin.addClickListener(e -> this.login());

		return loginForm;
	}

	private void login() {
		try {
			this.vaadinSharedSecurity.login(this.txtUsername.getValue(), this.txtPassword.getValue());
		} catch (AuthenticationException e) {
			this.setLoginErrorUI(e);
			e.printStackTrace();
		} catch (Exception e) {
			Notification.show("Oops, something happened! :/", Type.ERROR_MESSAGE);
			e.printStackTrace();
		}
	}

	private void setLoginErrorUI(AuthenticationException e) {
		if (this.logoutLabel != null) {
			this.logoutLabel.setVisible(false);
		}

		this.txtUsername.focus();
		this.txtUsername.selectAll();
		this.txtPassword.setValue("");
		this.loginFailedLabel.setValue("Sikertelen bejelentkezés: " + e.getMessage());
		this.loginFailedLabel.setVisible(true);
	}

}
