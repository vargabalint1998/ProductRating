package cyzz.springboot.studies.productRating.vaadin.view;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.AuthenticationException;

import com.vaadin.data.Binder;
import com.vaadin.data.validator.EmailValidator;
import com.vaadin.event.ShortcutAction;
import com.vaadin.navigator.View;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.Panel;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import cyzz.springboot.studies.productRating.bean.User;
import cyzz.springboot.studies.productRating.service.UserService;

@SpringView(name = RegisterView.VIEW_NAME)
public class RegisterView extends Panel implements View {
	private static final long serialVersionUID = -2727519864292335986L;
	public static final String VIEW_NAME = "registerView";

	private UserService userService;

	private TextField txtUsername;
	private TextField txtPassword;
	private TextField txtEmail;
	private TextField txtPasswordConfirm;
	private Button btnRegister;

	@Autowired
	public RegisterView(UserService userService) {
		this.userService = userService;
	}

	@PostConstruct
	public void init() {
		FormLayout registerForm = this.createRegisterForm();

		VerticalLayout loginLayout = this.createRegisternLayout(registerForm);

		VerticalLayout rootLayout = new VerticalLayout(loginLayout);
		rootLayout.setSizeFull();
		rootLayout.setComponentAlignment(loginLayout, Alignment.MIDDLE_CENTER);
		this.setContent(rootLayout);
		this.setSizeFull();
	}

	private VerticalLayout createRegisternLayout(FormLayout registerForm) {
		VerticalLayout loginLayout = new VerticalLayout();
		loginLayout.addComponent(registerForm);
		loginLayout.setComponentAlignment(registerForm, Alignment.TOP_CENTER);

		return loginLayout;
	}

	private FormLayout createRegisterForm() {
		FormLayout loginForm = new FormLayout();
		loginForm.setSizeUndefined();
		loginForm.setSpacing(true);

		this.txtUsername = new TextField("Felhasználónév");
		this.txtEmail = new TextField("Email cím");
		this.txtPassword = new PasswordField("Jelszó");
		this.txtPasswordConfirm = new PasswordField("Jelszó újra");
		this.btnRegister = new Button("Regisztráció");

		Binder<User> binder = new Binder<>(User.class);

		binder.forField(this.txtUsername).asRequired("Kötelező mező")
				.withValidator(str -> str.length() >= 4, "Minimum 4 karakterből kell állnia").bind("username");

		binder.forField(this.txtEmail).asRequired("Kötelező mező")
				.withValidator(new EmailValidator("Helytelen email cím")).bind("email");

		binder.forField(this.txtPassword).asRequired().withValidator(str -> str.length() >= 8, "Min 8 karakter")
				.bind("password");

		binder.forField(this.txtPasswordConfirm).asRequired()
				.withValidator(str -> str.equals(this.txtPassword.getValue()), "Nem egyezik a két beírt jelszó")
				.bind("password");

		loginForm.addComponents(this.txtUsername, this.txtEmail, this.txtPassword, this.txtPasswordConfirm,
				this.btnRegister);

		this.btnRegister.addStyleName(ValoTheme.BUTTON_PRIMARY);

		this.btnRegister.setClickShortcut(ShortcutAction.KeyCode.ENTER);

		User user = new User();

		binder.readBean(user);

		this.btnRegister.addClickListener(e -> {
			if (binder.writeBeanIfValid(user)) {
				this.register(user);
			}

		});

		return loginForm;
	}

	private void register(User user) {
		try {

			if (this.userService.register(user).equals("ok")) {
				Notification.show("Sikeres regisztráció! \n Aktiválja a felhasználót ", Type.HUMANIZED_MESSAGE);
			} else {
				Notification.show("Létezik ilyen felhasználó!", Type.ERROR_MESSAGE);
			}

		} catch (AuthenticationException e) {
			this.setLoginErrorUI(e);
			e.printStackTrace();
		} catch (Exception e) {

			Notification.show("Oops, something happened! :/", Type.ERROR_MESSAGE);
			e.printStackTrace();
		}

	}

	private void setLoginErrorUI(AuthenticationException e) {
		this.txtUsername.focus();
		this.txtUsername.selectAll();
		this.txtPassword.setValue("");
		Notification.show("Sikertelen regisztráció: ", e.getMessage(), Type.ERROR_MESSAGE);
	}

}
