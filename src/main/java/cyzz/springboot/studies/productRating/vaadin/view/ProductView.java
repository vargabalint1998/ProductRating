package cyzz.springboot.studies.productRating.vaadin.view;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.vaadin.dialogs.ConfirmDialog;
import org.vaadin.spring.security.shared.VaadinSharedSecurity;
import org.vaadin.teemu.ratingstars.RatingStars;

import com.vaadin.navigator.View;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.renderers.ButtonRenderer;

import cyzz.springboot.studies.productRating.bean.Product;
import cyzz.springboot.studies.productRating.bean.Review;
import cyzz.springboot.studies.productRating.service.ProductService;
import cyzz.springboot.studies.productRating.service.RoleService;
import cyzz.springboot.studies.productRating.service.UserService;
import cyzz.springboot.studies.productRating.util.SecurityUtils;
import cyzz.springboot.studies.productRating.vaadin.window.ProductInspectWindow;

@SpringView(name = ProductView.VIEW_NAME)
public class ProductView extends Panel implements View {

	/**
	 *
	 */
	private static final long serialVersionUID = -5798458045171676579L;
	public static final String VIEW_NAME = "productView";

	private ProductService service;
	private ApplicationContext appCtx;

	@Autowired
	private SecurityUtils utils;

	private Grid<Product> grid;

	@Autowired
	RoleService roleService;

	@Autowired
	VaadinSharedSecurity secu;

	@Autowired
	UserService userService;

	@Autowired
	public ProductView(ProductService service, ApplicationContext appCtx) {
		this.service = service;
		this.appCtx = appCtx;
	}

	@PostConstruct
	public void init() {
		this.loadProductGrid();
		this.refreshGrid();
		this.setContent(this.createMainLayout());
		this.setCaption("Termékek listája");
	}

	private Component createMainLayout() {
		VerticalLayout mainLayout = new VerticalLayout();
		mainLayout.setSpacing(true);
		mainLayout.setMargin(true);
		this.setSizeFull();

		mainLayout.addComponents(this.createSearchLayout(), this.grid);

		return mainLayout;
	}

	private Component createSearchLayout() {
		HorizontalLayout searchLayout = new HorizontalLayout();
		searchLayout.setDefaultComponentAlignment(Alignment.BOTTOM_LEFT);

		TextField txtName = new TextField("Termék neve");

		Button btnSearch = new Button("Keresés");
		btnSearch.addClickListener(e -> this.refreshGridByProduct(txtName.getValue()));

		searchLayout.addComponents(txtName, btnSearch);

		return searchLayout;
	}

	private void refreshGridByProduct(String value) {
		this.grid.setItems(this.service.findByName(value));
	}

	private void refreshGrid() {
		this.grid.setItems(this.service.listEnabledProducts());
	}

	private void loadProductGrid() {
		this.grid = new Grid<>(Product.class);
		this.grid.setSizeFull();
		this.grid.setColumnOrder("id", "name", "price", "buyLocation", "buyDate");
		this.grid.removeColumn("reviews");
		this.grid.removeColumn("description");
		this.grid.getColumn("id").setCaption("Azonosító");
		this.grid.getColumn("name").setCaption("Név");
		this.grid.getColumn("price").setCaption("Ár");
		this.grid.getColumn("buyLocation").setCaption("Vásárlás helye");
		this.grid.getColumn("buyDate").setCaption("Vásárlás dátuma");

		this.grid.addColumn(e -> "Megtekintés", new ButtonRenderer<>(r -> {
			ProductInspectWindow window = this.appCtx.getBean(ProductInspectWindow.class);
			window.init(r.getItem());
			UI.getCurrent().addWindow(window);

		}));

		boolean isAdmin = this.utils.hasRole("ADMIN");

		if (isAdmin) {
			this.grid.addColumn(e -> "Törlés", new ButtonRenderer<>(r -> {
				ConfirmDialog.show(UI.getCurrent(), "Törlés", "Biztos törölni szeretnéd?", "Igen", "Mégse", window -> {
					if (window.isConfirmed()) {
						Product prod = r.getItem();
						this.service.deleteProduct(prod);
						this.refreshGrid();
					}
				});
			}));
		} else {
			this.grid.removeColumn("id");
		}

		// TODO: Beleerőszakolni a grid-be a star view-et.

		this.grid.addComponentColumn(rating -> {
			RatingStars ratingStars = new RatingStars();
			ratingStars.setMaxValue(5);

			double ertek = 0;
			List<Review> rev = new ArrayList<>();
			rev = rating.getReviews();

			if (!rev.isEmpty()) {
				for (Review review : rev) {
					if (review.getStarCount() == null) {
						review.setStarCount(0);
					}
					ertek += review.getStarCount().doubleValue();
				}
				ratingStars.setValue(ertek / rev.size());
			} else {
				ratingStars.setValue(ertek);
			}
			ratingStars.setReadOnly(true);

			return ratingStars;
		}).setCaption("Átlagos értékelés");

	}
}
