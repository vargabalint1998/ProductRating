package cyzz.springboot.studies.productRating.vaadin.view;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.vaadin.spring.security.shared.VaadinSharedSecurity;

import com.vaadin.navigator.View;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Panel;
import com.vaadin.ui.UI;

import cyzz.springboot.studies.productRating.service.ProductService;
import cyzz.springboot.studies.productRating.vaadin.component.ProductFormLayout;

@SpringView(name = AddProductView.VIEW_NAME)
public class AddProductView extends Panel implements View {

	/**
	 *
	 */
	private static final long serialVersionUID = -1317129276808314617L;
	public static final String VIEW_NAME = "addProductView";

	ProductService service;
	@Autowired
	VaadinSharedSecurity secu;

	@Autowired
	public AddProductView(ProductService service) {
		this.service = service;
	}

	@PostConstruct
	public void init() {
		ProductFormLayout form = new ProductFormLayout(prod -> {
			this.service.save(prod);
			UI.getCurrent().getNavigator().navigateTo(ProductView.VIEW_NAME);
		});

		this.setContent(form);
		this.setCaption("Árú hozzáadása");
	}

}
