package cyzz.springboot.studies.productRating.vaadin.ui;

import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.spring.navigator.SpringNavigator;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import cyzz.springboot.studies.productRating.vaadin.view.AddProductView;
import cyzz.springboot.studies.productRating.vaadin.view.LoginView;
import cyzz.springboot.studies.productRating.vaadin.view.ProductView;
import cyzz.springboot.studies.productRating.vaadin.view.RegisterView;

@SpringUI(path = "/login")
public class LoginUI extends UI {

	private static final long serialVersionUID = 1645485833888101367L;

	@Autowired
	private SpringNavigator navigator;

	@Override
	protected void init(VaadinRequest request) {
		VerticalLayout mainLayout = new VerticalLayout();
		HorizontalLayout navbar = this.buildNav();
		VerticalLayout navigationLayout = new VerticalLayout();
		mainLayout.addComponents(navbar, navigationLayout);
		this.setContent(mainLayout);

		this.navigator.init(this, navigationLayout);

		if (this.navigator.getState().isEmpty() || this.navigator.getState().equals(ProductView.VIEW_NAME)
				|| this.navigator.getState().equals(AddProductView.VIEW_NAME)) {
			this.navigator.navigateTo(LoginView.VIEW_NAME);
		}

		this.setNavigator(this.navigator);

	}

	private HorizontalLayout buildNav() {
		HorizontalLayout nav = new HorizontalLayout();
		nav.setSizeFull();
		Button btnLogin = new Button();
		btnLogin.setCaption("Bejelentkezés");
		btnLogin.addClickListener(e -> this.navigator.navigateTo(LoginView.VIEW_NAME));

		Button btnRegister = new Button();
		btnRegister.setCaption("Regisztráció");
		btnRegister.addClickListener(e -> this.navigator.navigateTo(RegisterView.VIEW_NAME));

		Button btnLostPass = new Button("Elfelejtett jelszó",
				e -> Notification.show("Not implement yet", Type.WARNING_MESSAGE));

		btnLogin.addStyleName(ValoTheme.BUTTON_LINK);
		btnRegister.addStyleName(ValoTheme.BUTTON_LINK);
		btnLostPass.addStyleName(ValoTheme.BUTTON_LINK);

		nav.addComponents(btnLogin, btnRegister, btnLostPass);
		nav.setComponentAlignment(btnLostPass, Alignment.MIDDLE_RIGHT);
		nav.setComponentAlignment(btnLogin, Alignment.MIDDLE_LEFT);
		nav.setComponentAlignment(btnRegister, Alignment.MIDDLE_LEFT);

		return nav;
	}

}
