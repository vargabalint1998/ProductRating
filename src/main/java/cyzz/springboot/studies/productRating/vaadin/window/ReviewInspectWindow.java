package cyzz.springboot.studies.productRating.vaadin.window;

import org.springframework.context.annotation.Scope;

import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Window;

import cyzz.springboot.studies.productRating.bean.Review;
import cyzz.springboot.studies.productRating.vaadin.component.ReviewFormLayout;
import cyzz.springboot.studies.productRating.vaadin.component.ReviewFormLayout.SaveHandler;

@SpringComponent
@Scope("prototype")
public class ReviewInspectWindow extends Window {

	public void init(Review review, SaveHandler saveHandler) {

		ReviewFormLayout form = new ReviewFormLayout(review, saveHandler);

		this.setHeight("70%");
		this.setWidth("70%");
		this.center();
		this.setModal(true);

		this.setContent(form);

	}

}
