package cyzz.springboot.studies.productRating.vaadin.ui;

import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.Component;
import com.vaadin.ui.Grid;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.renderers.ButtonRenderer;

import cyzz.springboot.studies.productRating.bean.Product;
import cyzz.springboot.studies.productRating.service.ProductService;

@SpringUI(path = "/mod")
public class ModUI extends UI {

	Grid<Product> grid;

	@Autowired
	ProductService productService;

	@Override
	protected void init(VaadinRequest request) {
		this.loadGrid();
		this.refreshGrid();

		this.setContent(this.createMainLayout());
	}

	private Component createMainLayout() {
		VerticalLayout mainLayout = new VerticalLayout();
		mainLayout.setSpacing(true);
		mainLayout.setMargin(true);
		this.setSizeFull();

		mainLayout.addComponents(this.grid);

		return mainLayout;
	}

	private void refreshGrid() {
		this.grid.setItems(this.productService.listNotEnabledProducts());
	}

	private void loadGrid() {
		this.grid = new Grid<>(Product.class);
		this.grid.setSizeFull();

		this.grid.addColumn(e -> "Elfogad", new ButtonRenderer<>(r -> {
			this.productService.setEnabled(r.getItem(), true);
			this.refreshGrid();

		}));

		this.grid.addColumn(e -> "Elutasít", new ButtonRenderer<>(r -> {
			this.productService.deleteProduct(r.getItem());
			this.refreshGrid();
		}));

	}

}
