package cyzz.springboot.studies.productRating.vaadin.ui;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.renderers.ButtonRenderer;

import cyzz.springboot.studies.productRating.bean.User;
import cyzz.springboot.studies.productRating.service.UserService;
import cyzz.springboot.studies.productRating.vaadin.window.UserEditWindow;

@SpringUI(path = "/admin")
public class AdminUI extends UI {

	/**
	 *
	 */
	private static final long serialVersionUID = -4048703789298729659L;

	Grid<User> grid;

	@Autowired
	private UserService service;

	@Autowired
	private ApplicationContext appCtx;

	@Override
	protected void init(VaadinRequest request) {

		this.loadGrid();
		this.refreshGrid();

		this.setContent(this.createMainLayout());

	}

	private void loadGrid() {
		this.grid = new Grid<>(User.class);
		this.grid.setSizeFull();
		this.grid.setColumnOrder("id", "username", "email");
		this.grid.removeColumn("password");
		this.grid.removeColumn("activation");
		this.grid.getColumn("id").setCaption("Azonosító");
		this.grid.getColumn("username").setCaption("Név");
		this.grid.getColumn("email").setCaption("Email");

		this.grid.removeColumn("reviews");
		this.grid.removeColumn("accountNonExpired");
		this.grid.removeColumn("accountNonLocked");
		this.grid.removeColumn("credentialsNonExpired");

		this.grid.addColumn(e -> "Szerkesztés", new ButtonRenderer<>(r -> {
			UserEditWindow window = this.appCtx.getBean(UserEditWindow.class);
			window.init(r.getItem(), user -> {
				this.service.save(user);
			});
			UI.getCurrent().addWindow(window);
		}));

	}

	private Component createMainLayout() {
		VerticalLayout mainLayout = new VerticalLayout();
		mainLayout.setSpacing(true);
		mainLayout.setMargin(true);
		this.setSizeFull();

		mainLayout.addComponents(this.createSearchLayout(), this.grid);

		return mainLayout;
	}

	private Component createSearchLayout() {
		HorizontalLayout searchLayout = new HorizontalLayout();
		searchLayout.setDefaultComponentAlignment(Alignment.BOTTOM_LEFT);

		TextField txtName = new TextField("Felhasználó neve");

		Button btnSearch = new Button("Keresés");
		btnSearch.addClickListener(e -> this.refreshGridByUser(txtName.getValue()));

		searchLayout.addComponents(txtName, btnSearch);

		return searchLayout;
	}

	private void refreshGridByUser(String value) {
		this.grid.setItems(this.service.findByName(value));
	}

	private void refreshGrid() {
		this.grid.setItems(this.service.listUsers());
	}
}