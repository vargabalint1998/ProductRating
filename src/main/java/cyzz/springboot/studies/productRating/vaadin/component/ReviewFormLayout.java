package cyzz.springboot.studies.productRating.vaadin.component;

import org.vaadin.teemu.ratingstars.RatingStars;

import com.vaadin.data.Binder;
import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;

import cyzz.springboot.studies.productRating.bean.Review;

public class ReviewFormLayout extends FormLayout {
	private Review review;
	private SaveHandler saveHandler;

	public ReviewFormLayout(SaveHandler saveHandler) {
		this(new Review(), saveHandler);
	}

	public ReviewFormLayout(Review review, SaveHandler saveHandler) {
		this.review = review;
		this.saveHandler = saveHandler;

		this.loadContent();
	}

	private void loadContent() {
		this.setMargin(true);
		this.setSpacing(true);

		TextField txtId = new TextField("Azonosító");
		TextArea txtDescription = new TextArea("Leírás");
		RatingStars stars = new RatingStars();
		stars.setCaption("rating stats");

		txtId.setReadOnly(true);

		Binder<Review> binder = new Binder<>(Review.class);

		binder.forField(txtId).withNullRepresentation("")
				.withConverter(this::convertStringToInteger, this::convertIntegerToString).bind("id");

		binder.forField(txtDescription).withNullRepresentation("")
				.withValidator(str -> str.length() <= 256, "A mező hossza nem lehet több 256 karakternél.")
				.bind("description");

		// TODO: értékelés beállítása

		stars.setMaxValue(5);
		binder.forField(stars).withNullRepresentation(0.d)
				.withConverter(this::convertDoubleToInteger, this::convertIntegerToDouble).bind("starCount");

		if (this.review == null) {
			this.review = new Review();
			System.out.println("NULL REVIEW");
		}

		System.out.println(this.review);

		binder.readBean(this.review);

		Button btnSave = new Button("Mentés");
		btnSave.addClickListener(e -> {
			if (binder.writeBeanIfValid(this.review)) {
				this.saveHandler.onSave(this.review);
			}
		});

		this.addComponents(txtId, txtDescription, stars, btnSave);

	}

	private double convertIntegerToDouble(Integer i) {
		if (i != null) {
			return i;
		} else {
			return 0;
		}
	}

	private Integer convertDoubleToInteger(double d) {

		return new Integer((int) d);
	}

	private String convertIntegerToString(Integer i) {
		if (i != null) {
			return i.toString();
		}
		return null;
	}

	private Integer convertStringToInteger(String s) {
		if ((s != null) && !s.isEmpty()) {
			return Integer.valueOf(s);
		}
		return null;
	}

	@FunctionalInterface
	public interface SaveHandler {
		public void onSave(Review review);
	}

}
