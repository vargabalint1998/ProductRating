package cyzz.springboot.studies.productRating.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cyzz.springboot.studies.productRating.service.UserService;

@RestController
@RequestMapping("/activation")
public class RequestController {

	private UserService userService;

	@Autowired
	public RequestController(UserService userService) {
		this.userService = userService;
	}

	@GetMapping("/{code}")
	public void activation(@PathVariable(required = true) String code, HttpServletResponse response)
			throws IOException {
		if (code != null) {
			System.out.println("User aktiválás" + this.userService + "/" + code);
			this.userService.activate(code);

		} else {
			System.out.println("code null");
		}

		response.sendRedirect("/");

	}

}
