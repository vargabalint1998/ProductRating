package cyzz.springboot.studies.productRating.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import cyzz.springboot.studies.productRating.bean.Review;

public interface ReviewDao extends JpaRepository<Review, Integer> {

}
