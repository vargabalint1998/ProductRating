package cyzz.springboot.studies.productRating.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import cyzz.springboot.studies.productRating.bean.User;

public interface UserDao extends JpaRepository<User, Long> {

	public Optional<User> findByUsername(String username);

	public String findByEmail(String email);

	public User findByActivation(String activation);
}
