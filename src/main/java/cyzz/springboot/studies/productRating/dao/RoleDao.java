package cyzz.springboot.studies.productRating.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import cyzz.springboot.studies.productRating.bean.UserRole;

public interface RoleDao extends JpaRepository<UserRole, Long> {
	public UserRole findByAuthority(String authority);

}
