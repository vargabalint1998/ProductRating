package cyzz.springboot.studies.productRating.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import cyzz.springboot.studies.productRating.bean.Product;

public interface ProductDao extends JpaRepository<Product, Integer> {
	public List<Product> findByNameContains(String name);

	public List<Product> findByEnabled(boolean enabled);

}
