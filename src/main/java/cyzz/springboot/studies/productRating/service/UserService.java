package cyzz.springboot.studies.productRating.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import cyzz.springboot.studies.productRating.bean.User;
import cyzz.springboot.studies.productRating.bean.UserRole;
import cyzz.springboot.studies.productRating.dao.UserDao;

@Service
public class UserService implements UserDetailsService {

	private UserDao userDao;
	private EmailService emailService;
	private RoleService roleDao;

	@Autowired
	public UserService(UserDao userDao, EmailService emailService, RoleService roleDao) {
		this.userDao = userDao;
		this.emailService = emailService;
		this.roleDao = roleDao;
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		return this.userDao.findByUsername(username)
				.orElseThrow(() -> new UsernameNotFoundException("User not found: " + username));
	}

	public String register(User user) {
		if (this.userDao.findByUsername(user.getUsername()).isPresent()) {
			System.out.println("Létezik.");
			return "alreadyExist";
		}

		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		user.setPassword(encoder.encode(user.getPassword()));
		user.setActivation(RandomStringUtils.randomAlphanumeric(16));
		this.emailService.sendActivationEmail(user.getEmail(), user.getActivation());

		String role = "USER";

		UserRole userRole = this.roleDao.findByAuthority(role);
		if (userRole == null) {
			userRole = new UserRole(role);
		}

		this.roleDao.save(userRole);
		user.addRole(userRole);
		this.userDao.save(user);

		return "ok";
	}

	public void activate(String code) {
		User user = this.userDao.findByActivation(code);
		if (user == null) {
			System.out.println("user not found");
			// TODO:exception kezelés
		}
		user.setEnabled(true);
		user.setAccountNonExpired(true);
		user.setAccountNonLocked(true);
		user.setCredentialsNonExpired(true);
		user.setActivation("");
		this.userDao.save(user);
	}

	public User getUser(String username) {
		return this.userDao.findByUsername(username).get();
	}

	public Collection<User> findByName(String value) {
		List<User> users = new ArrayList<>();
		users.add(this.userDao.findByUsername(value).get());
		return users;
	}

	public Collection<User> listUsers() {
		return this.userDao.findAll();
	}

	public void save(User user) {
		this.userDao.save(user);
	}

}
