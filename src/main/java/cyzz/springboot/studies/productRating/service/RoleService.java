package cyzz.springboot.studies.productRating.service;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cyzz.springboot.studies.productRating.bean.UserRole;
import cyzz.springboot.studies.productRating.dao.RoleDao;

@Service
public class RoleService {

	@Autowired
	RoleDao dao;

	public Set<UserRole> getRoles() {
		Set<UserRole> set = new HashSet<>();
		set.addAll(this.dao.findAll());
		return set;
	}

	public void save(UserRole userRole) {
		this.dao.save(userRole);

	}

	public UserRole findByAuthority(String string) {
		return this.dao.findByAuthority(string);
	}

}
