package cyzz.springboot.studies.productRating.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class EmailService {

	JavaMailSender javaMailSender;

	private String basePath = "http://localhost:8889/";

	@Value("${spring.mail.username}")
	private String MESSAGE_FROM;

	@Autowired
	public EmailService(JavaMailSender javaMailSender) {
		this.javaMailSender = javaMailSender;
	}

	public void sendActivationEmail(String email, String activation) {
		SimpleMailMessage message = null;

		try {
			message = new SimpleMailMessage();
			message.setFrom(this.MESSAGE_FROM);
			message.setTo(email);
			message.setSubject("Sikeres regisztrálás");
			message.setText("Kedves " + email
					+ "! \n \n Köszönjük, hogy regisztráltál az oldalunkra! \n Kérjük kattints az alábbi linkre a regisztráció befejezéséhez: "
					+ this.basePath + "activation/" + activation);
			this.javaMailSender.send(message);

		} catch (Exception e) {
			// log.error("Hiba e-mail küldéskor az alábbi címre: " + email + " " + e);
			System.out.println("hiba email küldés: " + e.getMessage());

		}

	}

	public String getBasePath() {
		return this.basePath;
	}

	public void setBasePath(String basePath) {
		this.basePath = basePath;
	}

}
