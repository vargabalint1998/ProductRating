package cyzz.springboot.studies.productRating.service;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cyzz.springboot.studies.productRating.bean.Product;
import cyzz.springboot.studies.productRating.bean.Review;
import cyzz.springboot.studies.productRating.dao.ProductDao;

@Service
public class ProductService {
	@Autowired
	private ProductDao dao;

	@Autowired
	private ReviewService service;

	public List<Product> findByName(String value) {

		return this.dao.findByNameContains(value);
	}

	public List<Product> listNotEnabledProducts() {
		return this.dao.findByEnabled(false);
	}

	public void deleteProduct(Product prod) {
		List<Review> reviews = prod.getReviews();
		for (Review review : reviews) {
			this.service.deleteReview(review);
		}
		prod.setReviews(null);
		this.dao.delete(prod);
	}

	public void save(Product prod) {
		this.dao.save(prod);
	}

	public Collection<Product> listEnabledProducts() {
		return this.dao.findByEnabled(true);
	}

	public void setEnabled(Product item, boolean b) {
		item.setEnabled(b);
		this.dao.save(item);
	}

}
