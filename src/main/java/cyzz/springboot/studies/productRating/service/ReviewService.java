package cyzz.springboot.studies.productRating.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cyzz.springboot.studies.productRating.bean.Review;
import cyzz.springboot.studies.productRating.dao.ReviewDao;

@Service
public class ReviewService {

	@Autowired
	private ReviewDao dao;

	public void deleteReview(Review review) {
		review.getProduct().getReviews().remove(review);
		review.setProduct(null);
		this.dao.delete(review);
	}

	public List<Review> listReviews() {
		return this.dao.findAll();
	}

	public void updateReview(Review review) {
		this.dao.save(review);
	}

}
